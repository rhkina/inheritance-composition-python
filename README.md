# A Study about Inheritance and Composition in Python

## Introduction

I've been studying inheritance and composition a lot and I finally understand the inheritance subclass explosion problem!

There are many articles and videos about this subject but it is very confusing!

To understand the problem, let's go to the source: the GoF book, [Design Patterns: Elements or Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Object-Oriented-Addison-Wesley-Professional-ebook-dp-B000SEIBB8/dp/B000SEIBB8/ref=mt_other?_encoding=UTF8&me=&qid=) ([ref.1](#ref1)):


>*The two most common techniques for reusing functionality in object-oriented systems are class inheritance and object composition. As we've explained, class inheritance lets you define the implementation of one class in terms of another's. Reuse by subclassing is often referred to as white-box reuse. The term "white-box" refers to visibility: With inheritance, the internals of parent classes are often visible to subclasses.*
>
>*Object composition is an alternative to class inheritance. Here, new functionality is obtained by assembling or composing objects to get more complex functionality.*
>
>*Object composition requires that the objects being composed have well-defined interfaces. This style of reuse is called black-box reuse, because no internal details of objects are visible. Objects appear only as "black boxes."*
>
>*Inheritance and composition each have their advantages and disadvantages. Class inheritance is defined statically at compile-time and is straightforward to use, since it's supported directly by the programming language. Class inheritance also makes it easier to modify the implementation being reused. When a subclass overrides some but not all operations, it can affect the operations it inherits as well, assuming they call the overridden operations.*
>
>*But class inheritance has some disadvantages, too. First, you can't change the implementations inherited from parent classes at run-time, because inheritance is defined at compile-time. Second, and generally worse, parent classes often define at least part of their subclasses' physical representation. Because inheritance exposes a subclass to details of its parent's implementation, it's often said that "inheritance breaks encapsulation" [ref.2](#ref2). The implementation of a subclass becomes so bound up with the implementation of its parent class that any change in the parent's implementation will force the subclass to change.*
>
>*Implementation dependencies can cause problems when you're trying to reuse a subclass. Should any aspect of the inherited implementation not be appropriate for new problem domains, the parent class must be rewritten or replaced by something more appropriate? This dependency limits flexibility and ultimately reusability. One cure for this is to inherit only from abstract classes, since they usually provide little or no implementation.*
>
>*Object composition is defined dynamically at run-time through objects acquiring references to other objects. Composition requires objects to respect each others' interfaces, which in turn requires carefully designed interfaces that don't stop you from using one object with many others. But there is a payoff. Because objects are accessed solely through their interfaces, we don't break encapsulation. Any object can be replaced at run-time by another as long as it has the same type. Moreover, because an object's implementation will be written in terms of object interfaces, there are substantially fewer implementation dependencies. Object composition has another effect on system design. Favoring object composition over class inheritance helps you keep each class encapsulated and focused on one task. Your classes and class hierarchies will remain small and will be less likely to grow into unmanageable monsters. On the other hand, a design based on object composition will have more objects (if fewer classes), and the system's behavior will depend on their interrelationships instead of being defined in one class.*
>
>*That leads us to our second principle of object-oriented design:*
>
>> ***Favor object composition over class inheritance.***
>
> *Ideally, you shouldn't have to create new components to achieve reuse. You should be able to get all the functionality you need just by assembling existing components through object composition. But this is rarely the case, because the set of available components is never quite rich enough in practice. Reuse by inheritance makes it easier to make new components that can be composed with old ones. Inheritance and object composition thus work together.*
>
>*Nevertheless, our experience is that designers overuse inheritance as a reuse technique, and designs are often made more reusable (and simpler) by depending more on object composition. You'll see object composition applied again and again in the design patterns.*

I am pretty sure most of you understand inheritance very well, but do not understand why you should change your structural pattern to composition.

I had the same problem! I've seen some weird examples that does not clarify this at all!

So, two happenings made me understand and I want to share them with you.

1. Original book - to start understand the composite pattern I went to the origin and bought the GoF book, [Design Patterns: elements of reusable object-oriented software](#ref1). The Chapter 4 contains the Composite structural pattern description and the Graphic illustration help me a lot to understand this pattern!

<a name="figure1"></a>
<center>

![image info](./static/img/graphicUML.png)<br>
Figure 1. UML for Graphic class

</center>

2. Mattias Petter Johansson [video](https://youtu.be/wfMtDGfHWpA) from his *Fun Fun Function* channel! His example is so simple and so clarifying that I decided to reproduce here.

So, the following examples are mostly based on Mattias Petter Johansson [video](https://youtu.be/wfMtDGfHWpA) ([ref. 3](#ref3)), but using Python language instead of Javascript.

## Inheritance

Let's start talking about inheritance. Suppose that we are designing a game with dogs and cats playing all around. So you decide to create a `Dog` and a `Cat` class, and instantiate some dog and cat:

```python
class Dog(Animal):
    def __init__(self):
        pass

    def bark(self):
        print('\U0001F436'+' Woof')

class Cat(Animal):
    def __init__(self):
        pass

    def meow(self):
        print('\U0001F63B'+' Meow')

print()
snoopy = Dog()
snoopy.bark()

print()
garfield = Cat()
garfield.meow()
```

If you run this, you will get:

```bash
$ python inheritance.py

🐶 Woof

🐱 Meow
```

That is great! But, now, your project manager comes and says he needs our dogs and cats to run, eat, and poop!

OK! You could add three more functions (`run`, `eat` and `poop`) on each class (`Cat` and `Dog`), but these functions would be duplicated, and you come with a better idea: let's create a super class named `Animal` above the `Dog` and `Cat`! It seems a good idea, because in the future we can add other animals!

So, the code becomes like this:

```python
class Animal():
    def __init__(self):
        pass
    
    def eat(self):
        print('Yummy Yummy!')

    def poop(self):
        print("\U0001F4A9")

    def run(self):
        print('Running fast and furious!') 

class Dog(Animal):
    def __init__(self):
        pass

    def bark(self):
        print('\U0001F436'+' Woof')

class Cat(Animal):
    def __init__(self):
        pass

    def meow(self):
        print('\U0001F431'+' Meow')

print()
snoopy = Dog()
snoopy.bark()
snoopy.run()
snoopy.eat()
snoopy.poop()

print()
garfield = Cat()
garfield.meow()
garfield.run()
garfield.eat()
garfield.poop()
```

And the result is great, as expected!

```bash
$ python inheritance.py

🐶 Woof
Running fast and furious!
Yummy Yummy!
💩

🐱 Meow
Running fast and furious!
Yummy Yummy!
💩
```

Now, the game is becoming exciting, and the project manager wants robots! A killer robot and a cleaning robot to turn the game more interesting.

OK, that's easy, and as we plan to add other different robots in the future whe should create a Robot super class and the inherited MovingRobot and KillerRobot classes like this:

```python
class Robot():
    def __init__(self):
        pass
    
    def move(self):
        print('\U0001F916'+' Drrrrr... Moving all around!')

class CleaningRobot(Robot):
    def __init__(self):
        pass

    def clean(self):
        print('\U0001F916'+'\U0001F9F9'+' Vrooom! I am a vacuum cleaner robot!')

class KillerRobot(Robot):
    def __init__(self):
        pass

    def kill(self):
        print('\U0001F916'+'\U0001F52B'+' Dziiit, dziit! I am a killer robot!')

print()
zeek = CleaningRobot()
zeek.move()
zeek.clean()

print()
zoid = KillerRobot()
zoid.move()
zoid.kill()
```

If you run just above program you will get:

```bash
$ python inheritance.py

🤖 Drrrrr... Moving all around!
🤖🧹 Vrooom! I am a vacuum cleaner robot!

🤖 Drrrrr... Moving all around!
🤖🔫 Dziiit, dziit! I am a killer robot!
```

OK! Now your game is really exciting: you have a dog, a cat, a moving robot and a killer robot.

Then your project manager comes with another exciting idea: let's create a killer dog robot!!! :gun: :dog: :robot:

Now you're really stucked! You are pretty sure this object should be inherited from `Robot` class, but how could you inherit the `Dog` characteristics from the `Animal` class? Should you duplicate `Dog` methods inside `Robot` class?! :stuck_out_tongue_closed_eyes:

Nop, there is a better idea: **Composition**!

## Composition

As Mattias explains in his [video](https://youtu.be/wfMtDGfHWpA),

> **Inheritance** is when you design your types after what they **ARE** while **Composition** is when you design your types after what they **DO**.

The [Figure 1.](#figure1) from original [Design Patterns book](#ref1) also is very illustrative! 

Instead of creating classes of "what they are" we create one class of "what they do", but we do not use inheritance, but composition, like this:


```python
class Actions():
    
    def bark(self):
        print('\U0001F436'+' Woof')
    
    def meow(self):
        print('\U0001F431'+' Meow')

    def eat(self):
        print('Yummy Yummy!')

    def poop(self):
        print("\U0001F4A9")

    def run(self):
        print('Running fast and furious!') 

    def move(self):
        print('\U0001F916'+' Drrrrr... Moving all around!')

    def clean(self):
        print('\U0001F916'+'\U0001F9F9'+' Vrooom! I am a vacuum cleaner robot!')

    def kill(self):
        print('\U0001F916'+'\U0001F52B'+' Dziiit, dziit! I am a killer robot!')


class Dog():
    def __init__(self):
        self.actions = Actions()

    def bark(self):
        return self.actions.bark()

    def run(self):
        return self.actions.run()

    def eat(self):
        return self.actions.eat()

    def poop(self):
        return self.actions.poop()

class Cat():
    def __init__(self):
        self.actions = Actions()

    def meow(self):
        return self.actions.meow()

    def run(self):
        return self.actions.run()

    def eat(self):
        return self.actions.eat()

    def poop(self):
        return self.actions.poop()

class Superbot():

    def __init__(self):
        self.actions = Actions()

    def bark(self):
        return self.actions.bark()
    
    def kill(self):
        return self.actions.kill()

    def move(self):
        return self.actions.move()

print()
snoopy = Dog()
snoopy.bark()
snoopy.run()
snoopy.eat()
snoopy.poop()

print()
garfield = Cat()
garfield.meow()
garfield.run()
garfield.eat()
garfield.poop()

print()
henry = Superbot()
henry.bark()
henry.move()
henry.kill()
```

OK! I know, there are duplicated functions, but doing this way you will never be tempted to add unnecessary inheritances (class explosion problem) that could mess all your program! That's the point.

And if you run this programa you'll get:

```bash
$ python composition.py 

🐶 Woof
Running fast and furious!
Yummy Yummy!
💩

🐱 Meow
Running fast and furious!
Yummy Yummy!
💩

🐶 Woof
🤖 Drrrrr... Moving all around!
🤖🔫 Dziiit, dziit! I am a killer robot!
```

## Conclusion
>***Favor object composition over class inheritance.***


## References

<a name="ref1"></a> 1. Erich, Gamma et al. Design Patterns: elements of reusable object-oriented software, 1st. Edition, Addison-Wesley, 1995.

<a name="ref2"></a> 2. Alan Snyder. Encapsulation and inheritance in object-oriented languages. In *Object-Oriented Programming Systems, Languages, and Applications Conference Proceedings*, pages 38–45, Portland, OR, November 1986. ACM Press.

<a name="ref3"></a> 3. Fun Fun Function. *Composition over Inheritance.* Available at: [https://youtu.be/wfMtDGfHWpA](https://youtu.be/wfMtDGfHWpA) (Accessed: 16 December 2020)