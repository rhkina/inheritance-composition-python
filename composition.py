class Actions():
    
    def bark(self):
        print('\U0001F436'+' Woof')
    
    def meow(self):
        print('\U0001F431'+' Meow')

    def eat(self):
        print('Yummy Yummy!')

    def poop(self):
        print("\U0001F4A9")

    def run(self):
        print('Running fast and furious!') 

    def move(self):
        print('\U0001F916'+' Drrrrr... Moving all around!')

    def clean(self):
        print('\U0001F916'+'\U0001F9F9'+' Vrooom! I am a vacuum cleaner robot!')

    def kill(self):
        print('\U0001F916'+'\U0001F52B'+' Dziiit, dziit! I am a killer robot!')


class Dog():
    def __init__(self):
        self.actions = Actions()

    def bark(self):
        return self.actions.bark()

    def run(self):
        return self.actions.run()

    def eat(self):
        return self.actions.eat()

    def poop(self):
        return self.actions.poop()

class Cat():
    def __init__(self):
        self.actions = Actions()

    def meow(self):
        return self.actions.meow()

    def run(self):
        return self.actions.run()

    def eat(self):
        return self.actions.eat()

    def poop(self):
        return self.actions.poop()

class Superbot():

    def __init__(self):
        self.actions = Actions()

    def bark(self):
        return self.actions.bark()
    
    def kill(self):
        return self.actions.kill()

    def move(self):
        return self.actions.move()

print()
snoopy = Dog()
snoopy.bark()
snoopy.run()
snoopy.eat()
snoopy.poop()

print()
garfield = Cat()
garfield.meow()
garfield.run()
garfield.eat()
garfield.poop()

print()
henry = Superbot()
henry.bark()
henry.move()
henry.kill()
