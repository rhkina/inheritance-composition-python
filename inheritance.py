class Animal():
    def __init__(self):
        pass
    
    def eat(self):
        print('Yummy Yummy!')

    def poop(self):
        print("\U0001F4A9")

    def run(self):
        print('Running fast and furious!') 

class Dog(Animal):
    def __init__(self):
        pass

    def bark(self):
        print('\U0001F436'+' Woof')

class Cat(Animal):
    def __init__(self):
        pass

    def meow(self):
        print('\U0001F431'+' Meow')

print()
snoopy = Dog()
snoopy.bark()
snoopy.run()
snoopy.eat()
snoopy.poop()

print()
garfield = Cat()
garfield.meow()
garfield.run()
garfield.eat()
garfield.poop()

class Robot():
    def __init__(self):
        pass
    
    def move(self):
        print('\U0001F916'+' Drrrrr... Moving all around!')

class CleaningRobot(Robot):
    def __init__(self):
        pass

    def clean(self):
        print('\U0001F916'+'\U0001F9F9'+' Vrooom! I am a vacuum cleaner robot!')

class KillerRobot(Robot):
    def __init__(self):
        pass

    def kill(self):
        print('\U0001F916'+'\U0001F52B'+' Dziiit, dziit! I am a killer robot!')

print()
zeek = CleaningRobot()
zeek.move()
zeek.clean()

print()
zoid = KillerRobot()
zoid.move()
zoid.kill()
